<?php

declare(strict_types=1);

namespace thans\filesystem\driver;

use League\Flysystem\AdapterInterface;
use think\filesystem\Driver;
use thans\filesystem\adapter\ObsAdapter;
use thans\filesystem\traits\Storage;

class Hwcloud extends Driver
{
    use Storage;

    protected function createAdapter(): AdapterInterface
    {
        return new ObsAdapter($this->config);
    }
}
