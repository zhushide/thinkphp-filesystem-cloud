<?php

namespace thans\filesystem\adapter;

use League\Flysystem\Adapter\AbstractAdapter;
use League\Flysystem\Config;
use Obs\ObsClient;

class ObsAdapter extends AbstractAdapter
{
    private $obsClient;

    private $config;

    public function __construct($config = [])
    {
        $this->config = $config;

        $this->obsClient = new ObsClient($config);
    }

    public function write($path, $contents, Config $config)
    {
        $this->obsClient->putObject([
            'Bucket' => $this->config['bucket'],
            'Key' => $path,
            'Body' => $contents
        ]);

        return true;
    }

    public function writeStream($path, $resource, Config $config)
    {
        $this->obsClient->putObject([
            'Bucket' => $this->config['bucket'],
            'Key' => $path,
            'Body' => $resource
        ]);

        return true;
    }

    public function update($path, $contents, Config $config)
    {
        return $this->write($path, $contents, $config);
    }

    public function updateStream($path, $resource, Config $config)
    {
        return $this->writeStream($path, $resource, $config);
    }

    public function rename($path, $newpath)
    {
    }

    public function copy($path, $newpath)
    {
        return $this->obsClient->copyObject([
            'Bucket' => $this->config['bucket'],
            'Key' => $newpath,
            'CopySource' => $this->config['bucket'] . '/' . $path,
        ]) ? true : false;
    }

    public function delete($path)
    {
        return $this->obsClient->deleteObject([
            'Bucket' => $this->config['bucket'],
            'Key' => $path,
        ]) ? true : false;
    }

    public function deleteDir($dirname)
    {
    }

    public function createDir($dirname, Config $config)
    {
    }

    public function setVisibility($path, $visibility)
    {
    }

    public function has($path)
    {
        try {
            return (bool) $this->getMetadata($path);
        } catch (\Throwable $e) {
            return false;
        } catch (\Exception $e) {
            return false;
        }
    }

    public function read($path)
    {
        $contents = $this->obsClient->getObject([
            'Bucket' => $this->config['bucket'],
            'Key' => $path
        ]);

        return ['type' => 'file', 'path' => $path, 'contents' => $contents['Body']->getContents()];
    }

    public function readStream($path)
    {
        $contents = $this->obsClient->getObject([
            'Bucket' => $this->config['bucket'],
            'Key' => $path,
            'SaveAsStream' => true
        ]);

        return ['type' => 'file', 'path' => $path, 'stream' => $contents['Body']];
    }

    public function listContents($directory = '', $recursive = false)
    {
    }


    public function getMetadata($path)
    {
        return $this->obsClient->getObjectMetadata([
            'Bucket' => $this->config['bucket'],
            'Key' => $path
        ]);
    }

    public function getSize($path)
    {
        $meta = $this->getMetadata($path);
        return isset($meta['ContentLength']) ? ['size' => $meta['ContentLength']] : false;;
    }

    public function getMimetype($path)
    {
        $meta = $this->getMetadata($path);
        return isset($meta['ContentType']) ? ['mimetype' => $meta['ContentType']] : false;
    }


    public function getTimestamp($path)
    {
    }

    public function getVisibility($path)
    {
    }
}
